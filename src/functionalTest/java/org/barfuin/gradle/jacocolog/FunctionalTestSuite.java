/*
 * Copyright 2019 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;


/**
 * Functional tests for this plugin, which means tests that actually run Gradle via TestKit.
 */
public class FunctionalTestSuite
{
    @Rule
    public final TemporaryFolder testProjectDir = new TemporaryFolder();



    @Test
    public void testSetup1_Default()
        throws IOException
    {
        arrangeTestProject("setup1-default");
        final String actualLog = act(true, "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.6%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogTestCoverage");
    }



    @Test
    public void testSetup2_LineCoverageOnly()
        throws IOException
    {
        arrangeTestProject("setup2-lineCoverageOnly");
        final String actualLog = act(true, "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "Test Coverage:\n    - Line Coverage: 80%\n:jacocoLogTestCoverage");
    }



    @Test
    public void testSetup3_BrokenXml1()
        throws IOException
    {
        arrangeTestProject("setup3-brokenXml");
        final String actualLog = act(false,
            "-Pbarfuin_xmlFile=jacocoTestReport-broken1.xml", "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "Counter of type LINE not found in JaCoCo XML report");
    }



    @Test
    public void testSetup3_BrokenXml2()
        throws IOException
    {
        arrangeTestProject("setup3-brokenXml");
        final String actualLog = act(false,
            "-Pbarfuin_xmlFile=jacocoTestReport-broken2.xml", "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "No counters found in JaCoCo XML report");
    }



    @Test
    public void testSetup3_BrokenXml3()
        throws IOException
    {
        arrangeTestProject("setup3-brokenXml");
        final String actualLog = act(false,
            "-Pbarfuin_xmlFile=jacocoTestReport-broken3.xml", "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "Caused by: org.xml.sax.SAXParseException");
    }



    @Test
    public void testSetup4_MissingXml()
        throws IOException
    {
        arrangeTestProject("setup4-missingXml");
        final String actualLog = act(false, "check", "jacocoLogTestCoverage");

        assertLogged(actualLog, "JaCoCo XML report not found:");
    }



    @Test
    public void testSetup5_MultipleSourceSets()
        throws IOException
    {
        arrangeTestProject("setup5-multipleTestTasks");
        final String actualLog = act(true, "jacocoFooReport", "jacocoBarReport", "jacocoBazReport");

        // Assert that all three test tasks and the default test task have been executed and jacoco info created
        assertFileExists("build/jacoco/fooTest.exec");
        assertFileExists("build/jacoco/barTest.exec");
        assertFileExists("build/jacoco/bazTest.exec");
        assertFileAbsent("build/jacoco/test.exec");  // task 'test' was not executed

        // Assert that jacoco reports have been created for all test tasks (XML), HTML only when not disabled
        assertFileAbsent("build/reports/jacoco/jacocoFooReport/html");
        assertFileExists("build/reports/jacoco/jacocoFooReport/jacocoFooReport.xml");
        assertFileExists("build/reports/jacoco/jacocoBarReport/html");
        assertFileExists("build/reports/jacoco/jacocoBarReport/jacocoBarReport.xml");
        assertFileExists("build/reports/jacoco/jacocoBazReport/html");
        assertFileExists("build/reports/jacoco/jacocoBazReport/jacocoBazReport.xml");
        assertFileAbsent("build/reports/jacoco/jacocoTestReport");

        // Assert that correct log output has been created by our plugin
        assertLogged(actualLog, "Test Coverage:\n    - Class Coverage: 100%\n    - Method Coverage: 100%\n"
            + "    - Branch Coverage: 50%\n    - Line Coverage: 80%\n    - Instruction Coverage: 80.6%\n"
            + "    - Complexity Coverage: 57.1%\n:jacocoLogFooCoverage");
        assertLogged(actualLog, "Test Coverage:\n    - Branch Coverage: 50%\n    - Instruction Coverage: 80.6%\n"
            + ":jacocoLogBarCoverage");
        assertNumMatches(2, actualLog, "Test Coverage:\n");
    }



    private void arrangeTestProject(final String pName)
        throws IOException
    {
        final File srcDir = getSourceDir(pName);
        FileUtils.copyDirectory(srcDir, testProjectDir.getRoot());

        final File props = new File("build/tmp/testprops/gradle.properties");
        if (!props.canRead()) {
            Assert.fail("Prerequisite Gradle task 'testprops' not run - "
                + "execute functional tests via Gradle to ensure correct environment");
        }
        FileUtils.copyFileToDirectory(props, testProjectDir.getRoot());

        System.out.println("Test project set up in: " + testProjectDir.getRoot());
    }



    private File getSourceDir(final String pSetupName)
    {
        return new File("src/functionalTest/resources", pSetupName);
    }



    private String act(final boolean pExpectSuccess, final String... pCommandLineArgs)
    {
        final GradleRunner runner = GradleRunner.create();
        runner.forwardOutput();
        runner.withPluginClasspath();
        runner.withProjectDir(testProjectDir.getRoot());
        runner.withArguments(Stream.concat(
            Arrays.stream(new String[]{"--info", "--stacktrace"}),
            Arrays.stream(pCommandLineArgs)).toArray(String[]::new));
        //runner.withDebug(true);

        BuildResult buildResult = pExpectSuccess ? runner.build() : runner.buildAndFail();
        return buildResult.getOutput();
    }



    private void assertLogged(final String pActualLog, final String pExpectedLogOutput)
    {
        final String actualLogUnix = unixifyLog(pActualLog);
        Assert.assertTrue(actualLogUnix.contains(pExpectedLogOutput));
    }



    private void assertFileExists(final String pRelativePath)
    {
        Assert.assertTrue("Expected file or directory not found: " + pRelativePath,
            new File(testProjectDir.getRoot(), pRelativePath).canRead());
    }



    private void assertFileAbsent(final String pRelativePath)
    {
        Assert.assertFalse("File or directory found which should not be present: " + pRelativePath,
            new File(testProjectDir.getRoot(), pRelativePath).exists());
    }



    private void assertNumMatches(final int pExpectedCount, final String pActualLog, final String pSub)
    {
        final String actualLogUnix = unixifyLog(pActualLog);
        int count = 0;
        if (!actualLogUnix.isEmpty()) {
            for (int idx = 0; (idx = actualLogUnix.indexOf(pSub, idx)) != -1; idx += pSub.length()) {
                ++count;
            }
        }
        Assert.assertEquals(pExpectedCount, count);
    }



    private String unixifyLog(final String pActualLog)
    {
        Assert.assertNotNull(pActualLog);
        return pActualLog.replaceAll(Pattern.quote("\r\n") + '?', Matcher.quoteReplacement("\n"));
    }
}
